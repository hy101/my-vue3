import { createRouter, createWebHistory } from 'vue-router'
import Clusters from '@/views/Clusters/index.vue'
import PluginRepo from '@/views/PluginRepo/index.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      children: [
        {
          path: '',
          component: () => import('@/views/Layout/index.vue'),
          redirect: 'clusters',
          children: [
            {
              path: 'clusters',
              component: Clusters
            },
            {
              path: 'pluginRepo',
              component: PluginRepo
            }
          ]
        }
      ]
    }
  ],
  scrollBehavior() {
    return {
      top: 0
    }
  }
})

export default router
