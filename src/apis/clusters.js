import http from '@/utils/http'

export function getGroupListApi() {
  return http({
    url: '/node/groupList'
  })
}

export function uninstallPluginApi({ groupName, pluginId }) {
  return http({
    url: '/node/uninstallPlugin',
    method: 'POST',
    params: {
      groupName,
      pluginId
    }
  })
}

export function installPluginApi({ groupName, pluginId, version }) {
  return http({
    url: '/node/installPlugin',
    method: 'POST',
    params: {
      groupName,
      pluginId,
      version,
      sync: false
    }
  })
}

export function getPluginsApi({ groupName }) {
  return http({
    url: '/node/plugin',
    params: {
      groupName
    }
  })
}

export function listTasksByGroupApi(groupName) {
  return http({
    url: '/task/listTaskByGroup',
    method: 'POST',
    params: {
      groupName
    }
  })
}

// 提交任务
export function submitTaskApi(task) {
  return http({
    url: '/task/submit',
    method: 'POST',
    data: task
  })
}

export function deleteTaskApi(taskId) {
  return http({
    url: '/task/deleteTask',
    method: 'POST',
    params: {
      taskId
    }
  })
}
