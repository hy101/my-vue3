import http from '@/utils/http'

export function listPluginsApi() {
  return http({
    url: '/plugin/listPlugins'
  })
}
