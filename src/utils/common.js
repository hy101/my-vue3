export const toStringPluginDependencies = (dependencies, optional) => {
  dependencies = optional
    ? dependencies.filter((item) => item.optional)
    : dependencies.filter((item) => !item.optional)
  if (dependencies.length === 0) {
    return ''
  }
  const converted = dependencies.map((item) => convert(item)).join(', ')
  return optional ? `[${converted}]` : converted
}
const convert = (dependency) => {
  return dependency.pluginVersionSupport === '*'
    ? dependency.pluginId
    : dependency.pluginId + '@' + dependency.pluginVersionSupport
}

export const convertTaskState = (state) => {
  let result = ''
  switch (state) {
    case 'over':
      result = '结束'
      break
    case 'created':
      result = '已创建'
      break
  }
  return result
}

export const convertStateTagType = (state) => {
  let result = 'success'
  switch (state) {
    case 'over':
      result = 'danger'
      break
  }
  return result
}
