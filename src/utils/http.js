import axios from 'axios'

const axiosInstance = axios.create({
  // baseURL: 'http://192.168.0.7:8080/mydataharbor',
  baseURL: 'http://127.0.0.1:8080/mydataharbor',
  timeout: 5000
})

axiosInstance.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    return config
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

// 添加响应拦截器
axiosInstance.interceptors.response.use(
  function (response) {
    // 2xx 范围内的状态码都会触发该函数。
    // 对响应数据做点什么
    response = response.data
    return response
  },
  function (error) {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么
    return Promise.reject(error)
  }
)

export default axiosInstance
