import { defineStore } from 'pinia'
import { ref, computed } from 'vue'
import { getGroupListApi } from '@/apis/clusters'

export const useNodeGroupStore = defineStore('activeGroup', () => {
  const groupList = ref({})

  const getGroupList = async () => {
    const { data } = await getGroupListApi()
    groupList.value = data
  }

  const activeGroupName = ref('')
  const activeGroup = computed(() => {
    // 不能返回空值
    return groupList.value[activeGroupName.value] || {}
  })
  const setActiveGroup = (groupName) => {
    activeGroupName.value = groupName
  }
  return { activeGroupName, activeGroup, setActiveGroup, groupList, getGroupList }
})
