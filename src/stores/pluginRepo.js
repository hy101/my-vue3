import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { listPluginsApi } from '@/apis/pluginRepo'

export const usePluginRepoStore = defineStore('pluginRepo', () => {
  const pluginRepo = ref([])
  const listPlugins = async () => {
    const res = await listPluginsApi()
    pluginRepo.value = res.data['本地存储库']
  }

  const allPlugins = computed(() => {
    let result = []
    for (let i = 0; i < pluginRepo.value.length; i++) {
      result = [...result, ...pluginRepo.value[i].plugins]
    }
    return result
  })

  return {
    pluginRepo,
    listPlugins,
    allPlugins
  }
})
