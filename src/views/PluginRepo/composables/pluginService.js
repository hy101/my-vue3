import { ElMessage } from 'element-plus'
import { usePluginRepoStore } from '@/stores/pluginRepo'
import { storeToRefs } from 'pinia'

export const usePluginService = () => {
  const { listPlugins } = usePluginRepoStore()
  const { pluginRepo } = storeToRefs(usePluginRepoStore())
  const uploadSuccess = (resp) => {
    if (resp.code === -1) {
      ElMessage({
        message: resp.msg,
        type: 'error'
      })
    } else {
      ElMessage({
        message: '插件上传成功',
        type: 'success'
      })
      setTimeout(() => listPlugins(), 1000)
    }
  }
  return {
    pluginRepo,
    uploadSuccess
  }
}
